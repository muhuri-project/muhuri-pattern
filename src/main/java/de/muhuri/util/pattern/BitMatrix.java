package de.muhuri.util.pattern;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * BitMatrix
 */
public class BitMatrix {

    private static final class NotEquivEnforcer{
        Set<Integer> equivalent = new TreeSet<>();
        private int size;
        NotEquivEnforcer(int size){
            this.size = size;
        }

        //if the enforcer does not forbid the value, it is assumed, that the value is accepted outside and should be forbidden the next time it is asked.
        public boolean forbids(Integer i){
            if (equivalent.contains(i)) {
                return true;
            }
            BitMatrix bm = new BitMatrix(size);
            bm.setValue(i);
            List<Integer> currentEquivs = bm.getEquivalents();
            boolean match = false;
            for(Integer cE:currentEquivs){
                if (equivalent.contains(cE)) {
                    match=true;
                }
            }
            equivalent.addAll(currentEquivs);
            return match;
        }
    }

    private static class MinBitsEnforcer{

        private int size;
        private int minBits;

        MinBitsEnforcer(int size){
            this.size=size;
            this.minBits=size-1;
        }

        public boolean forbids(Integer i){
            BitMatrix bm = new BitMatrix(size);
            bm.setValue(i);

            return (bm.getSetBitCount()<minBits);
        }
    }

    private static class MinDimensionEnforcer{
        private int size;
        private int minDim;
        MinDimensionEnforcer(int size){
            this.size = size;
            minDim = Math.floorDiv(size, 2);
        }

        public boolean forbids(Integer i){
            BitMatrix bm = new BitMatrix(size);
            bm.setValue(i);

            return (Math.min(bm.getFilledColCount(), bm.getFilledRowCount())<minDim);
        }
    }

    public static List<Integer> findDistinctOfSize(int size) {
        NotEquivEnforcer equivEnf = new NotEquivEnforcer(size);
        MinBitsEnforcer minBitEnf = new MinBitsEnforcer(size);
        MinDimensionEnforcer minDimEnf = new MinDimensionEnforcer(size);

        List<Integer> distinct = new LinkedList<>();

        int maxValue = 1 << (size * size);
        for (int i = 0; i < maxValue; i++) {
            if(minBitEnf.forbids(i)){
                continue;
            }
            if(minDimEnf.forbids(i)){
                continue;
            }
            if(equivEnf.forbids(i)){
                continue;
            }
            

            distinct.add(i);
        }
        return distinct;
    }

    

    private int value;
    private int size;

    public BitMatrix(int size) {
        this.size = size;
    }

    public BitMatrix(int size, int value) {
        this.size = size;
        setValue(value);
    }

    public int getSize() {
        return size;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    private int getIndex(int row, int col) {
        if (row >= size || col >= size || row < 0 || col < 0) {
            throw new IndexOutOfBoundsException(
                    "BitMatrix with size " + size + " has no index at (" + row + "," + col + ")");
        }
        return row * size + col;
    }

    public List<Boolean> asBoolList() {
        List<Boolean> result = new LinkedList<>();
        
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                int index = getIndex(row, col);
                result.add(index, hasBitAtIndex(index));

            }
        }
        return result;
    }

    private int getSetBitCount() {
        int result = 0;
        
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                int index = getIndex(row, col);
                if(hasBitAtIndex(index)){
                    result++;
                }
            }
        }
        return result;
    }

    public String asText() {
        StringBuilder sb = new StringBuilder();
        boolean firstRow = true;
        for (int row = 0; row < size; row++) {
            if (firstRow) {
                firstRow = false;
            } else {
                sb.append("\n");
            }
            for (int col = 0; col < size; col++) {
                int index = getIndex(row, col);
                if (hasBitAtIndex(index)) {
                    sb.append('x');
                } else {
                    sb.append('o');
                }
            }

        }

        return sb.toString();
    }

    private boolean hasBitAtIndex(int index) {
        return (value & (1 << index)) == (1 << index);
    }

    private int setBitAtIndex(int original, int index) {
        return original | (1 << index);
    }

    private void transform(TargetIndexGetter tig) {
        int shiftedValue = 0;
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                int sourceI = getIndex(row, col);
                if (hasBitAtIndex(sourceI)) {
                    int targetI = tig.getTargetIndex(row, col);
                    shiftedValue = setBitAtIndex(shiftedValue, targetI);
                }
            }
        }
        setValue(shiftedValue);
    }

    public void shiftRight() {
        transform((row, col) -> {
            return getIndex(row, col + 1);
        });
    }

    public void shiftDown() {
        transform((row, col) -> {
            return getIndex(row + 1, col);
        });
    }

    public void rotate() {
        transform((row, col) -> {
            return getIndex(col, (size - 1) - row);
        });
    }

    private void shiftLeft() {
        transform((row, col) -> {
            return getIndex(row, col - 1);
        });
    }

    public void shiftUp() {
        transform((row, col) -> {
            return getIndex(row - 1, col);
        });
    }

    public void mirrorLR() {
        transform((row, col) -> {
            return getIndex(row, (size - 1) - col);
        });
    }

    public void mirrorTD() {
        transform((row, col) -> {
            return getIndex((size - 1)-row,  col);
        });
    }

    public void alignTopLeft() {
        alignTop();
        alignLeft();
    }

    private void alignTop() {
        if (value == 0) {
            return;
        }
        while (true) {
            try {
                shiftUp();
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }
    }

    private void alignLeft() {
        if (value == 0) {
            return;
        }
        while (true) {
            try {
                shiftLeft();
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }
    }

    public void removeEmptyRows(){
        if (value == 0) {
            return;
        }
        int removed  = 0;
        for (int row = 0; row < size; row++) {
            boolean isEmptyRow = isEmptyRow(row);
            if(isEmptyRow){   
                final int currentRow = row;        
                //shiftUp alle rows weiter unten    
                transform((rowParam, colParam) -> {
                    if(rowParam>currentRow){
                        return getIndex(rowParam - 1, colParam);
                    }
                    else{
                        return getIndex(rowParam , colParam);
                    }
                });
                removed++;
                if(removed>size){
                    return;
                }
                //die aktuelle Row nochmal prüfen (ist jetzt ja die von eins unten drunter)
                row--;
            }
        }
        
    }
    private int getFilledRowCount(){
        int count = size;
        for(int row=0;row<size;row++){
            if(isEmptyRow(row)){
                count--;
            }
        }
        return count;
    }

    private int getFilledColCount(){
        int count = size;
        for(int col=0;col<size;col++){
            if(isEmptyCol(col)){
                count--;
            }
        }
        return count;
    }

    private boolean isEmptyRow(int row) {
        boolean isEmptyRow = true;
        for (int col = 0; col < size; col++) {
            int sourceI = getIndex(row, col);
            if (hasBitAtIndex(sourceI)) {
                isEmptyRow  =false;  
                break;                  
            }
        }
        return isEmptyRow;
    }

    private boolean isEmptyCol(int col) {
        boolean isEmptyCol = true;
        for (int row = 0; row < size; row++) {
            int sourceI = getIndex(row, col);
            if (hasBitAtIndex(sourceI)) {
                isEmptyCol  =false;  
                break;                  
            }
        }
        return isEmptyCol;
    }

    public void removeEmptyCols(){
        rotate();
        removeEmptyRows();
        rotate();
        rotate();
        rotate();
    }

    public void diagonalEnlarge() {
        //erst growAround, dann mit transform die richtigen Zielkoordinaten.
        //sinnvoller: nur solche Muster zulassen, bei denen mindestens eine Ecke besetzt ist. Dann kann man die ausrichtung erkennen.
        //lohnt natürlich erst ab size>=4
    }
    
    private void growAround(){
        //rundrum eine leere spalte/reihe hinzufügen
    }

    private BitMatrix copy() {
        BitMatrix bm = new BitMatrix(size);
        bm.setValue(getValue());

        return bm;
    }

    private boolean addMissingEquivalents(List<Integer> knownEquivs, BitMatrix bm) {
        if (knownEquivs.contains(bm.getValue())) {
            return false;
        }

        knownEquivs.add(bm.getValue());
        try {
            bm.shiftRight();
            if (addMissingEquivalents(knownEquivs, bm)) {
                return true;
            }
        } catch (IndexOutOfBoundsException e2) {

        }

        bm.alignLeft();
        try {
            bm.shiftDown();
            if (addMissingEquivalents(knownEquivs, bm)) {
                return true;
            }
        } catch (IndexOutOfBoundsException e2) {

        }

        bm.rotate();
        bm.alignTopLeft();
        if (addMissingEquivalents(knownEquivs, bm)) {
            return true;
        }
        bm.mirrorLR();
        if (addMissingEquivalents(knownEquivs, bm)) {
            return true;
        }

        bm.mirrorTD();
        if (addMissingEquivalents(knownEquivs, bm)) {
            return true;
        }

        bm.removeEmptyCols();
        if (addMissingEquivalents(knownEquivs, bm)) {
            return true;
        }

        bm.removeEmptyRows();
        if (addMissingEquivalents(knownEquivs, bm)) {
            return true;
        }

        return true;
    }

    public List<Integer> getEquivalents() {
        BitMatrix bm = copy();

        List<Integer> equiv = new LinkedList<>();
        addMissingEquivalents(equiv, bm);

        return equiv;

    }

    private interface TargetIndexGetter {
        public int getTargetIndex(int row, int col);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + value;
        result = prime * result + size;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BitMatrix other = (BitMatrix) obj;
        if (value != other.value)
            return false;
        if (size != other.size)
            return false;
        return true;
    }

	

}