package de.muhuri.util.pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeThat;

import java.util.List;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;

public class BitMatrixTest {

    @Test
    public void testSimpleSetValueGetValue() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(3);
        assertEquals(3, bm.getValue());
    }

    @Test
    public void testBitOperations() {
        assertEquals(1 << 3, 8);

        assertEquals((9 & (1 << 0)), (1 << 0));
        assertEquals((9 & (1 << 1)), 0);
        assertEquals((9 & (1 << 2)), 0);
        assertEquals((9 & (1 << 3)), (1 << 3));
    }

    @Test
    public void testPrintAfterInit() {
        final BitMatrix bm = new BitMatrix(3);
        String txt = bm.asText();
        assertEquals("ooo\nooo\nooo", txt);
    }

    @Test
    public void testPrintDifferentValues() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1);
        assertEquals("xoo\nooo\nooo", bm.asText());

        bm.setValue(2);
        assertEquals("oxo\nooo\nooo", bm.asText());

        bm.setValue(16);
        assertEquals("ooo\noxo\nooo", bm.asText());

        bm.setValue(18);
        assertEquals("oxo\noxo\nooo", bm.asText());
    }

    @Test
    public void testPrintDifferentSizes() {
        final BitMatrix bm2 = new BitMatrix(2);
        bm2.setValue(1);
        assertEquals("xo\noo", bm2.asText());

        final BitMatrix bm4 = new BitMatrix(4);
        bm4.setValue(96);
        assertEquals("oooo\noxxo\noooo\noooo", bm4.asText());
    }

    @Test
    public void testShiftRight() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1);

        bm.shiftRight();

        assertEquals(2, bm.getValue());
        assertEquals("oxo\nooo\nooo", bm.asText());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testShiftRightOverflow() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(4);
        bm.shiftRight();
        System.out.println(bm.asText());
    }

    public void testShiftDown() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1);

        bm.shiftDown();

        assertEquals(8, bm.getValue());
        assertEquals("ooo\nxoo\nooo", bm.asText());

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testShiftDownOverflow() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(64);
        bm.shiftDown();
        System.out.println(bm.asText());
    }

    @Test
    public void testRotate() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(3);

        bm.rotate();

        assertEquals(36, bm.getValue());
        assertEquals("oox\noox\nooo", bm.asText());

    }

    @Test
    public void testAlignTopLeft() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(256 + 128 + 32);
        bm.alignTopLeft();

        assertEquals(26, bm.getValue());
        assertEquals("oxo\nxxo\nooo", bm.asText());
    }

    @Test
    public void testMirror(){
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(256 + 128 + 32);
        bm.mirrorLR();
        assertEquals(200, bm.getValue());
        assertEquals("ooo\nxoo\nxxo", bm.asText());

    }

    @Test
    @Ignore
    public void testDiagonalEnlarge(){
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(8+16+128);
        bm.diagonalEnlarge();
        assertEquals(81, bm.getValue());
        assertEquals("xoo\noxo\nxoo", bm.asText());

    }

    @Test
    public void testEquivalentListSingle() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1);

        List<Integer> equivalent = bm.getEquivalents();

        // alle Werte mit einem gesetzten Bit
        int checks = 0;
        for (int shift = 0; shift < 3 * 3; shift++) {
            assertTrue(equivalent + " does not contain " + (1 << shift), equivalent.contains(1 << shift));
            checks++;
        }
        assertEquals(equivalent.size(), checks);
    }

    @Test
    public void testEquivalentPattern() {
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1 + 2 + 4 + 8);
        List<Integer> equivalent = bm.getEquivalents();
        
        assertContainsAll(equivalent, 15, 120, 147, 294, 60, 480, 201, 402, 39, 312, 210, 420, 57, 456, 75, 150);

        
       
    }

    private void print(List<Integer> lst, int size){
        BitMatrix bm = new BitMatrix(size);
        for(int val:lst){
            bm.setValue(val);
            System.out.println("\n=====\n" + val + "\n" + bm.asText());
        }

        System.out.println("\n => " + lst.size() + " BitMatrix");
    }

    
    private void assertContainsAll(List<Integer> lst, int ... values){
        assertEquals(values.length, lst.size());
        for(int val:values){
            assertTrue(val + " erwartet", lst.contains(val));
        }
        
    }

    @Test
    public void testFindAllDistinct1(){
        List<Integer> distinct = BitMatrix.findDistinctOfSize(1);
        assertContainsAll(distinct, 0, 1);

    }

     @Test
    public void testFindAllDistinct2(){
        List<Integer> distinct = BitMatrix.findDistinctOfSize(2);
        //print(distinct, 2);
        
        assertContainsAll(distinct, 1, 3, 6,7,15);

    }

    @Test
    public void testFindAllDistinct3(){
        List<Integer> distinct = BitMatrix.findDistinctOfSize(3);
        //print(distinct, 3);
        assertEquals(68,distinct.size());        
    }

    @Test
    public void testFindAllDistinct4(){
        List<Integer> distinct = BitMatrix.findDistinctOfSize(4);
        //print(distinct, 4);
        assertEquals(6104,distinct.size());        
    }

    @Test
    public void testRemoveEmptyFirstRow(){
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(8 + 16 + 32 + 64 + 0 + 256);
        bm.removeEmptyRows();
        assertEquals(1+2+4+8+32, bm.getValue());
        assertEquals("xxx\nxox\nooo", bm.asText());
    }

    @Test
    public void testRemoveEmptyInnerRow(){
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1 + 2 + 4 + 64 + 0 + 256);
        bm.removeEmptyRows();
        assertEquals(1+2+4+8+32, bm.getValue());
        assertEquals("xxx\nxox\nooo", bm.asText());
    }

    @Test
    public void testRemoveTwoInnerRows(){
        final BitMatrix bm = new BitMatrix(4);
        bm.setValue(0+0+ 4 + 8 + 4096+0+0+0);
        bm.removeEmptyRows();
        assertEquals(4+8+16, bm.getValue());
        assertEquals("ooxx\nxooo\noooo\noooo", bm.asText());
    }

    @Test
    public void testRemoveInnerCol(){
        final BitMatrix bm = new BitMatrix(3);
        bm.setValue(1+0+0+8+0+32);
        bm.removeEmptyCols();
        assertEquals(1+8+16, bm.getValue());
        assertEquals("xoo\nxxo\nooo", bm.asText());
    }

}
   