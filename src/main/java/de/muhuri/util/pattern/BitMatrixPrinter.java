package de.muhuri.util.pattern;


import java.awt.image.BufferedImage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.management.RuntimeErrorException;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.ExpressionContext;
import org.thymeleaf.context.IContext;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

public class BitMatrixPrinter {
    /*
     * <svg viewBox="0 0 30 10" xmlns="http://www.w3.org/2000/svg"> <circle
     * id="myCircle" cx="5" cy="5" r="4" stroke="blue"/> <use href="#myCircle"
     * x="10" fill="blue"/> <use href="#myCircle" x="20" fill="white" stroke="red"/>
     * </svg>
     */
    final private ClassLoaderTemplateResolver tr = new ClassLoaderTemplateResolver();

    public String asSvg(BitMatrix bm) {
        TemplateEngine te = new TemplateEngine();
        te.setTemplateResolver(tr);

        Context ctx = new Context();
        ctx.setVariable("bm", bm);

        return te.process("singlepattern.svg", ctx);
    }

    public String asHtml(List<Integer> lst, final int size) {
        TemplateEngine te = new TemplateEngine();
        te.setTemplateResolver(tr);

        List<BitMatrix> bmList = lst.stream().map((Integer value) -> {
            return new BitMatrix(size, value);
        }).sorted((BitMatrix one, BitMatrix two) -> {
            return one.getValue() - two.getValue();
        }).collect(Collectors.toList());

        Context ctx = new Context();
        ctx.setVariable("bmList", bmList);
        ctx.setVariable("size", size);

        return te.process("RasterPattern.html", ctx);

    }

    public static void main(String... args) throws IOException {
        //printRaster(2);
        //printRaster(3);
        printRaster(4);
        printAllSingle(4);

        //printCharFun();

      //System.out.println( BarcodePrinter.asAscii("http://muhuri.de/qr/1"));

    }

    private static void printCharFun(){
        System.out.printf("%-7s %-43s %7s\t%s\t%7s%n",
                "Char", "Name", "Unicode", "UTF-8 encoded", "Decoded");
 
        for (int codepoint : new int[]{0x0041, 0x00F6, 0x0416, 0x20AC, 0x1D11E, 0x25A1, 0x2588}) {
            byte[] encoded = utf8encode(codepoint);
            Formatter formatter = new Formatter();
            for (byte b : encoded) {
                formatter.format("%02X ", b);
            }
            String encodedHex = formatter.toString();
            int decoded = utf8decode(encoded);
            System.out.printf("%-7c %-43s U+%04X\t%-12s\tU+%04X%n",
                    codepoint, Character.getName(codepoint), codepoint, encodedHex, decoded);
        }
    }

    public static byte[] utf8encode(int codepoint) {
        return new String(new int[]{codepoint}, 0, 1).getBytes(StandardCharsets.UTF_8);
    }
 
    public static int utf8decode(byte[] bytes) {
        return new String(bytes, StandardCharsets.UTF_8).codePointAt(0);
    }

    private static void printAllSingle(int size) throws IOException{
        File targetDir = Paths.get("./target").toAbsolutePath().toFile();
        File outDir = new File(targetDir, "BitMatrixSingles");
        if(!outDir.exists()){
            outDir.mkdirs();
        }


        BitMatrixPrinter printer = new BitMatrixPrinter();

        BitMatrix bm = new BitMatrix(size);
        List<Integer> values = BitMatrix.findDistinctOfSize(size);

        for(int i=0;i<values.size();i++){
            Integer val  =values.get(i);
            bm.setValue(val);
            String svg = printer.asSvg(bm);
            File file = new File(outDir, "bm" + size + "-" + i + ".svg");
            file.createNewFile();
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            try(BufferedWriter bw = new BufferedWriter(fw);)
            {
                        
                bw.write(svg);
            }catch(IOException e){
                throw new RuntimeException(e);
            }
            String targetUrl  = "https://muhuri.de/qr/" + i;
            String fileName  = "qr_" + i;

            BufferedImage img = BarcodePrinter.asImage(targetUrl);
            ImageIO.write(img, "png", new File(outDir, fileName + ".png"));
        }
    }

    private static void printRaster(int size) throws IOException {
        BitMatrixPrinter printer = new BitMatrixPrinter();
        File targetDir = Paths.get("./target").toAbsolutePath().toFile();
        File outFile = new File(targetDir, "BitMatrix" + size + ".html");
        outFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(outFile);
        try (OutputStreamWriter writer = new OutputStreamWriter(fos);) {
            writer.write(printer.asHtml(BitMatrix.findDistinctOfSize(size), size));
        } catch (Exception e) {
            //TODO: handle exception
        }        
    }
}