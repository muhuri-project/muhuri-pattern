package de.muhuri.util.pattern;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import uk.org.okapibarcode.backend.QrCode;
import uk.org.okapibarcode.output.Java2DRenderer;
import uk.org.okapibarcode.output.SvgRenderer;

public class BarcodePrinter {
    public static String asSvg(String data) {
        QrCode qr = new QrCode();
        qr.setContent(data);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        SvgRenderer renderer = new SvgRenderer(out, 1, Color.WHITE, Color.BLACK, false);

        try {
            renderer.render(qr);
            return out.toString("utf8");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static BufferedImage asImage(String data) {
        QrCode qr = new QrCode();
        qr.setContent(data);

        BufferedImage image = new BufferedImage(qr.getWidth(), qr.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
        Graphics2D g2d = image.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        Java2DRenderer renderer = new Java2DRenderer(g2d, 1, Color.WHITE, Color.BLACK);
        renderer.render(qr);

        return image;
    }

    
    public static String asAscii(String data){
        return asAscii(asImage(data));
    }
    private static String asAscii(BufferedImage img){
        
        StringBuilder sb = new StringBuilder();
        for(int x=0;x<img.getWidth()+2;x++){
            sb.append("\u2588");
        }
        sb.append("\n");
        for(int y=0;y<img.getHeight();y++){
            sb.append("\u2588");
            for(int x=0;x<img.getWidth();x++){
                int rgb = img.getRGB(x, y);
                switch (rgb) {
                    case -16777216:
                        sb.append(" ");
                        break;
                
                    default:
                        sb.append("\u2588");
                        break;
                }
            }
            sb.append("\u2588");
            sb.append("\n");
        }
        for(int x=0;x<img.getWidth()+2;x++){
            sb.append("\u2588");
        }
        return sb.toString();
    }
}
